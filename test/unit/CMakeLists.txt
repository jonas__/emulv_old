cmake_minimum_required(VERSION 3.14)
project(UNIT_TESTING)

# GoogleTest requires at least C++14
set(CMAKE_CXX_STANDARD 14)

include(FetchContent)
FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/03597a01ee50ed33e9dfd640b249b4be3799d395.zip
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

enable_testing()

add_executable(
  unit_tests

  gpio/test_configuration.cpp
  gpio/test_output.cpp
  gpio/test_input.cpp

  uart/test_configuration.cpp
  uart/test_transmitter.cpp
  uart/test_receiver.cpp

  ../../src/utils/events/EventEmitter.cpp
  ../../src/modules/gpio.cpp
  ../../src/modules/uart.cpp
)
target_link_libraries(
  unit_tests

  GTest::gtest_main
)

include(GoogleTest)
gtest_discover_tests(unit_tests)
