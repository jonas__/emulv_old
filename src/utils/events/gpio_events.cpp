/**
 * Represents implementation of declarations in gpio.h header file related to GPIO state change events.
 * 
 * @author Stanislav Kafara
 * @version 2023-04-27
 */

#include "gpio_events.h"
